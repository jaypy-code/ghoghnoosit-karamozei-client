import { Component, OnInit } from '@angular/core';
import { MdcDialog, MdcSnackbar } from '@angular-mdc/web';
import { ConfirmationComponent } from '../../includes/confirmation/confirmation.component';
import { Http } from '../../../../services/http/http.service';
import { Toast } from '../../../../services/toast/toast.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  public fields: string[] = ['کامپیوتر', 'گرافیک'];
  public field = this.fields[0];
  public questions = null;
  public mode: string = 'table';
  public data: any = {};
  constructor(private http: Http, private toast: Toast, private dialog: MdcDialog, private snackbar: MdcSnackbar) { }

  ngOnInit() {
    this.fetch(this.fields[0]);
  }

  fetch(field:any){
    if(field.value) {
      field = field.value;
    }
        
    this.http.grapql('main', `{ questions(field: "${field}") { question answers answer } }`)
    .then((res:any)=>{            
      this.questions = res['data']['questions'];
    })
  }

  setEditable(data) {
    if (data == null) {
      data = {
        question: '',
        answers: ['', '', '', ''],
        answer: -1,
        field: ''
      }
    }
    this.data = data;
    this.mode = 'edit';
  }

  setAnswer(index=-1){
    this.data['answer'] = index;
  }

  submit(){
    let method = this.data['id']?'PUT':'POST';
    let header = this.data['id']?{ id: this.data['id'] }:{};
    this.http.request('main', '/question', method,this.data, true, header)
    .then((res: any) => {
      this.toast.make(res['message']['fa'], this.snackbar);
      if (res['status'] == true) {
        this.data = null;
        this.mode = 'table';
      }
    })
  }
}
