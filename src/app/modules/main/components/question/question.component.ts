import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';
import { Router } from '@angular/router';
import { Account } from '../../../../services/account/account.service';
import { Http } from '../../../../services/http/http.service';
import { Toast } from '../../../../services/toast/toast.service';
import { Loading } from '../../../../services/loading/loading.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  public question: number = 0;
  public questions: object[] = [];
  public answer: number = -1;
  public answers: object[] = [];
  constructor(private http: Http, private toast: Toast, private account: Account, private loading: Loading, private snackbar: MdcSnackbar, private router: Router) { }

  ngOnInit() {
    if(this.account.info['answerd'] == false){
      this.fetch();
    } else {
      this.router.navigate(['/panel/dashboard']);
      this.account.info['answerd'] = true;
    }
  }

  fetch(){    
    this.http.grapql('main', `{ questions(field: "${this.account.info['field']}") { id question answers } }`)
    .then((res:any)=>{            
      this.questions = res['data']['questions'];
    })
  }

  setAnswer(index=-1){
    this.answer = index;
  }

  submit(){
    this.answers.push({ answer: this.answer, question: this.questions[this.question]['id'] });
    if(this.questions.length==this.question+1){
      this.loading.show = true;    
      this.http.request('main', '/answer', 'POST', { answer: this.answers }, true)
      .then((res:any)=>{
        this.toast.make(res['message']['fa'], this.snackbar);
        if(res['status'] == true){
          this.router.navigate(['/panel/dashboard']);
          this.account.info['answerd'] = true;
        }
      })  
    } else {
      this.answer = -1;
      this.question += 1;
    }
  }
}
