import { Component, OnInit } from '@angular/core';
import { Http } from '../../../../services/http/http.service';
import { Account } from '../../../../services/account/account.service';
import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit {

  public data = {
    "user": {},
    "answer": [{ answer: 0, question: { answer: 0 } }]
  }
  constructor(private http: Http, public account: Account, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.fetch();
  }

  fetch(){
    let id = this.activatedRoute.params['_value']['id'];
    this.http.request('main', `/answer?id=${id}`, 'GET', {}, true)
    .then((res:any)=>{
      this.data = res['data'];   
      this.setScore();   
    })
  }

  setScore(){
    let score = 0;
    for(let i in this.data['answer']){
      let answer = this.data['answer'][i];
      if(answer['answer'] == answer['question']['answer']) score++;
    }
    var ctx = (<HTMLCanvasElement>document.getElementById('score')).getContext('2d');
    new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ['امتیاز بدست آورده', 'امتیاز از دست داده'],
        datasets: [{
          data: [score, this.data.answer.length - score],
          backgroundColor: [
            "#2196f3",
            "#6ec6ff"
          ],
          borderWidth: 0
        }]
      }
    });
  }
}
